import numpy as np

# From data_utils.py in 3d-baseline repo
H36M_NAMES = ['']*32
H36M_NAMES[0]  = 'Hip'
H36M_NAMES[1]  = 'RHip'
H36M_NAMES[2]  = 'RKnee'
H36M_NAMES[3]  = 'RFoot'
H36M_NAMES[6]  = 'LHip'
H36M_NAMES[7]  = 'LKnee'
H36M_NAMES[8]  = 'LFoot'
H36M_NAMES[12] = 'Spine'
H36M_NAMES[13] = 'Thorax'
H36M_NAMES[14] = 'Neck'  # 'Neck/Nose'
H36M_NAMES[15] = 'Head'
H36M_NAMES[17] = 'LShoulder'
H36M_NAMES[18] = 'LElbow'
H36M_NAMES[19] = 'LWrist'
H36M_NAMES[25] = 'RShoulder'
H36M_NAMES[26] = 'RElbow'
H36M_NAMES[27] = 'RWrist'

# # Somehow the above mapping doesn't work for me. It's completely off.
# # Below is my own mapping that I figured out by watching raw mocap point
# # data in puppeteer:

# H36M_NAMES = ['']*32
# H36M_NAMES[0]  = 'RKnee'
# H36M_NAMES[1]  = 'RFoot'
# H36M_NAMES[2]  = 'LHip'
# H36M_NAMES[3]  = 'LKnee'
# H36M_NAMES[6]  = 'LFoot'
# H36M_NAMES[7]  = 'Spine'
# H36M_NAMES[8]  = 'Neck'
# H36M_NAMES[12] = 'Thorax'
# H36M_NAMES[13] = 'Head'
# H36M_NAMES[14] = 'LShoulder'
# H36M_NAMES[15] = 'LElbow'
# H36M_NAMES[17] = 'LWrist'
# H36M_NAMES[18] = 'RShoulder'
# H36M_NAMES[19] = 'RElbow'
# H36M_NAMES[25] = 'RWrist'
# H36M_NAMES[26] = 'Hip'
# H36M_NAMES[27] = 'RHip'

# names = set(H36M_NAMES)
# names.remove('')
# names = list(names)

names = [name for name in H36M_NAMES if name != '']

assert len(names) == 17

def _rthigh_calculate_scale_factor(p, actual_rthigh=0.40416340032958814):
    """Find scale factor s (ratio between right thigh length in
    neural network prediction and actual measured right thigh length)
    for rescaling 3D pose estimation results to meters."""
    t = p3d.shape[0]
    rthighs = np.zeros(t)
    for i in range(t):
        rthighs[i] = np.linalg.norm(p3d[i, 1] - p3d[i, 2])  # Distances between right knee and right hip through time
    rthigh = rthighs.mean()
    scale_factor = actual_rthigh / rthigh
    return scale_factor

def calculate_scale_factor(p, actual_height):
    """Find scale factor s (ratio between height in
    neural network prediction and actual measured height of the subject)
    for rescaling 3D pose estimation results to meters."""
    t = p.shape[0]
    heights = np.zeros(t)
    lfoot = p[:, H36M_NAMES.index('LFoot')]
    rfoot = p[:, H36M_NAMES.index('RFoot')]
    betweenfeet = (lfoot + rfoot) / 2
    head = p[:, H36M_NAMES.index('Head')]
    for i in range(t):
        heights[i] = np.linalg.norm(head[i] - betweenfeet[i])
    height = heights.mean()
    scale_factor = actual_height / height
    print(height)
    return scale_factor

def recenter(x):
    """Shift floating base to the origin (0, 0, 0)"""
    y = np.empty_like(x)
    for i in range(x.shape[0]):
        y[i] = x[i] - x[i, 0]
    y[:, 0, :] = 0.0
    return y

p3dfloating = np.load('./p3d.npy')  # (t, 32, 3)
p3d = recenter(p3dfloating)

# actual_rthigh = 0.40416340032958814  # subject-specific value, taken from subject's HeiMan parametrization.
# scale_factor = calculate_scale_factor(p3d, actual_rthigh=actual_rthigh)
# actual_height = 1.835 - 0.087  #  - Total height minus ankle height because keypoints are on ankles, not floor
actual_height = 1.835

scale_factor = calculate_scale_factor(p3d, actual_height=actual_height)
print(scale_factor)

sp3d = scale_factor * p3d

# sp3d = np.load('sp3d.npy')  # (t, 32, 3)

import ezc3d

c3 = ezc3d.c3d()

pts = []
for i in range(32):
    name = H36M_NAMES[i]
    if name != '':
        pts.append(sp3d[:, i, :])

points = np.array(pts) * 1000  # (17, t, 3)
points = np.moveaxis(points, -1, 0)  # (3, 17, t)

# The fourth dimension is not used here, so it's filled with ones
ones = np.ones((1, *points.shape[1:]), dtype=points.dtype)
points4d = np.concatenate((points, ones), axis=0)
print(points4d.shape)

# Fill with data
rate = 25
scale = -1
# c3['header']['points']['frame_rate'] = rate
c3['parameters']['POINT']['RATE']['value'] = [rate]
c3['parameters']['POINT']['SCALE']['value'] = [scale]  # TODO: What's up with this?
c3['parameters']['POINT']['LABELS']['value'] = names
c3['data']['points'] = points4d
c3['data']['points'][3] = 1.

# Write the data
# c3.write('/tmp/nn.c3d')
c3.write('./nn.c3d')

# import IPython ; IPython.embed(); exit()
