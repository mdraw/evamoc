Evamo code
==========

Code and small files for the bachelor's thesis "Evaluating reconstruction capabilities of motion capture systems for humans" at the ORB lab, Uni Heidelberg.

This repository uses git submodules, so use `git clone --recursive` to clone it.

(C) 2019 Martin Drawitsch

Credits:

- The code in `model/HeiMan` is copied from https://github.com/martinfelis/puppeteer/tree/dc83187b/HeiMan, only the degrees of freedom of the joints have been changed in HeiMan.lua
