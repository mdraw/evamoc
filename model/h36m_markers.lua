-- -- Manual marker assignments based on h36m joint keypoints
-- -- TODO: Refine positions
-- markers_h36m = {
--     Pelvis = {
--         Hip = {0.0, 0.0, 0.0,},
--     },
--     ThighRight = {
--         RHip = {0.0, 0.0, 0.0,},
--     },
--     ShankRight = {
--         RKnee = {0.0, 0.0, 0.0,},
--     },
--     FootRight = {
--         RFoot = {0.0, 0.0, 0.0,},
--     },
--     ThighLeft = {
--         LHip = {0.0, 0.0, 0.0,},
--     },
--     ShankLeft = {
--         LKnee = {0.0, 0.0, 0.0,},
--     },
--     FootLeft = {
--         LFoot = {0.0, 0.0, 0.0,},
--     },
--     MiddleTrunk = {
--         Spine = {0.0, 0.0, 0.248 / 2},  -- + (MiddleTrunkHeight / 2) in z direction
--     },
--     UpperTrunk = {
--         -- Spine = {0.0, 0.0, 0.0,},
--     },
--     ClaviculaRight = {
--     },
--     UpperArmRight = {
--         RShoulder = {0.0, 0.0, 0.0,},
--     },
--     LowerArmRight = {
--         RElbow = {0.0, 0.0, 0.0,},
--     },
--     HandRight = {
--         RWrist = {0.0, 0.0, 0.0,},
--     },
--     ClaviculaLeft = {
--     },
--     UpperArmLeft = {
--         LShoulder = {0.0, 0.0, 0.0,},
--     },
--     LowerArmLeft = {
--         LElbow = {0.0, 0.0, 0.0,},
--     },
--     HandLeft = {
--         LWrist = {0.0, 0.0, 0.0,},
--     },
--     Neck = {
--         Thorax = {0.0, 0.0, 0.0,},
--     },
--     Head = {
--         -- Neck = {0.074, 0.0, 0.0},
--         -- Head = {0.0, 0.0, 0.0},
--         Head = {0.0, 0.0, 0.2688},  -- HeiMan subject-specific HeadLength
--     }
-- }

-- markers_h36m = {
--     Pelvis = {
--         Hip = {0.0, 0.0, 0.0,},
--     },
--     ThighRight = {
--         RHip = {0.0, 0.0, 0.0,},
--     },
--     ShankRight = {
--         RKnee = {0.0, 0.0, -0.1,},
--     },
--     FootRight = {
--         RFoot = {0.0, 0.0, 0.0,},
--     },
--     ThighLeft = {
--         LHip = {0.0, 0.0, 0.0,},
--     },
--     ShankLeft = {
--         LKnee = {0.0, 0.0, -0.1,},
--     },
--     FootLeft = {
--         LFoot = {0.0, 0.0, 0.0,},
--     },
--     MiddleTrunk = {
--         Spine = {0.0, 0.0, 0.248 / 2},  -- + (MiddleTrunkHeight / 2) in z direction
--     },
--     UpperTrunk = {
--         -- Spine = {0.0, 0.0, 0.0,},
--     },
--     ClaviculaRight = {
--     },
--     UpperArmRight = {
--         RShoulder = {0.0, 0.1, -0.1,},
--     },
--     LowerArmRight = {
--         RElbow = {0.0, 0.0, -0.1,},
--     },
--     HandRight = {
--         RWrist = {0.0, 0.0, -0.1,},
--     },
--     ClaviculaLeft = {
--     },
--     UpperArmLeft = {
--         LShoulder = {0.0, -0.1, -0.1,},
--     },
--     LowerArmLeft = {
--         LElbow = {0.0, 0.0, -0.1,},
--     },
--     HandLeft = {
--         LWrist = {0.0, 0.0, -0.1,},
--     },
--     Neck = {
--         -- Thorax = {0.0, 0.0, -0.2,},
--     },
--     Head = {
--         -- Neck = {0.074, 0.0, 0.0},
--         -- Head = {0.0, 0.0, 0.0},
--         Head = {0.09, 0.0, 0.0},  -- HeiMan subject-specific HeadLength
--     }
-- }

markers_h36m = {
    Pelvis = {
        Hip = {0.0, 0.0, 0.1,},
    },
    ThighRight = {
        RHip = {0.0, 0.0, 0.1,},
    },
    ShankRight = {
        RKnee = {0.0, 0.0, 0.0,},
    },
    FootRight = {
        RFoot = {0.0, 0.0, -0.09,},
    },
    ThighLeft = {
        LHip = {0.0, 0.0, 0.1,},
    },
    ShankLeft = {
        LKnee = {0.0, 0.0, 0.0,},
    },
    FootLeft = {
        LFoot = {0.0, 0.0, -0.09,},
    },
    MiddleTrunk = {
        Spine = {0.0, 0.0, 0.248 / 2},  -- + (MiddleTrunkHeight / 2) in z direction
    },
    UpperTrunk = {
        -- Spine = {0.0, 0.0, 0.0,},
    },
    ClaviculaRight = {
    },
    UpperArmRight = {
        RShoulder = {0.0, 0.1, 0.0,},
    },
    LowerArmRight = {
        RElbow = {0.0, 0.0, 0.0,},
    },
    HandRight = {
        RWrist = {0.0, 0.0, 0.0,},
    },
    ClaviculaLeft = {
    },
    UpperArmLeft = {
        LShoulder = {0.0, -0.1, 0.0,},
    },
    LowerArmLeft = {
        LElbow = {0.0, 0.0, 0.0,},
    },
    HandLeft = {
        LWrist = {0.0, 0.0, 0.0,},
    },
    Neck = {
        Thorax = {0.0, 0.0, 0.0,},
    },
    Head = {
        -- Neck = {0.074, 0.0, 0.0},
        -- Head = {0.0, 0.0, 0.0},
        Head = {0.07, 0.0, 0.1},  -- HeiMan subject-specific HeadLength
    }
}