-- Heiman-based subject-specific model of Kevin
-- - with information from body measurements for Xsens
--   and segment lengths calculated from static c3d

-- Depending on the marker_type, this will assign either Qualisys, Xsens or h36m (NN) markers
-- Unfortunately we can't export all markers in one model because fitting capture data to models
-- currently.

-- Choose markers to be added:

marker_type = os.getenv("MARKER_TYPE")

if marker_type == nil then
    marker_type = "xsens"
    marker_type = "qualisys"
    marker_type = "h36m"
    -- marker_type = "all"  -- TODO

end

-- load HeiMan and additional helper modules
HeiMan = require 'HeiMan.HeiMan'
serialize = require 'HeiMan.serialize'

-- create actual HeiMan instance by specifying the mass and height
heiman = HeiMan (73, 1.835)

-- modify parameters

heiman.parameters["ClaviculaLeftLength"]     = 0.218 -- == shoulder width / 2
heiman.parameters["ClaviculaRightLength"]    = 0.218
heiman.parameters["FootLeftHeight"]          = 0.087  -- == ankle height
heiman.parameters["FootLeftLength"]          = 0.261
-- heiman.parameters["FootLeftMass"]            = 1.013800
-- heiman.parameters["FootLeftPosteriorPoint"]  = 0.069600
heiman.parameters["FootRightHeight"]         = 0.087
heiman.parameters["FootRightLength"]         = 0.261
-- heiman.parameters["FootRightMass"]           = 1.013800
-- heiman.parameters["FootRightPosteriorPoint"] = 0.069600
heiman.parameters["HandLeftLength"]          = 0.195  -- from SegLen.txt
-- heiman.parameters["HandLeftMass"]            = 0.451400
heiman.parameters["HandRightLength"]         = 0.195  -- from SegLen.txt
-- heiman.parameters["HandRightMass"]           = 0.451400
heiman.parameters["HeadLength"]              = 0.2688  -- from SegLen.txt
-- heiman.parameters["HeadMass"]                = 5.135600
heiman.parameters["HipLeftWidth"]            = 0.115
heiman.parameters["HipRightWidth"]           = 0.115
heiman.parameters["LowerArmLeftLength"]      = 0.282 -- from SegLen.txt
-- heiman.parameters["LowerArmLeftMass"]        = 1.198800
heiman.parameters["LowerArmRightLength"]     = 0.282 -- from SegLen.txt
-- heiman.parameters["LowerArmRightMass"]       = 1.198800
-- heiman.parameters["LowerTrunkHeight"]        = 0.145616
-- heiman.parameters["LowerTrunkMass"]          = 8.265800
heiman.parameters["MiddleTrunkHeight"]       = 0.248
-- heiman.parameters["MiddleTrunkMass"]         = 12.084200
heiman.parameters["NeckLength"]              = 0.072  -- from Antro.txt
-- heiman.parameters["NeckMass"]                = 0.495800
heiman.parameters["ShankLeftLength"]         = 0.428  -- knee height - ankle height
-- heiman.parameters["ShankLeftMass"]           = 3.204200
heiman.parameters["ShankRightLength"]        = 0.428
-- heiman.parameters["ShankRightMass"]          = 3.204200
-- heiman.parameters["ShoulderLeftHeight"]      = 0.170602
-- heiman.parameters["ShoulderRightHeight"]     = 0.170602
-- heiman.parameters["SuprasternaleHeight"]     = 0.170602
heiman.parameters["ThighLeftLength"]         = 0.408  -- hip height - knee height
-- heiman.parameters["ThighLeftMass"]           = 10.478400
heiman.parameters["ThighRightLength"]        = 0.408
-- heiman.parameters["ThighRightMass"]          = 10.478400
heiman.parameters["UpperArmLeftLength"]      = 0.283  -- from SegLen.txt
-- heiman.parameters["UpperArmLeftMass"]        = 2.005400
heiman.parameters["UpperArmRightLength"]     = 0.283  -- from SegLen.txt
-- heiman.parameters["UpperArmRightMass"]       = 2.005400
-- heiman.parameters["UpperTrunkAltHeight"]     = 0.2444  -- from SegLen.txt - unused
-- heiman.parameters["UpperTrunkAltMass"]       = 11.810400
heiman.parameters["UpperTrunkHeight"]        = 0.1726  -- from SegLen.txt and Antro.txt (UpperTrunk minus ShoulderNeckOffset)
-- heiman.parameters["UpperTrunkMass"]          = 11.810400


-- create the actual model (must be called whenever parameters were modified)
model = heiman:create_model()


-- Get the full path of the directory that contains this script file
scriptdir_path = debug.getinfo(1).source:match("@(.*)$"):match("(.*/)")


if marker_type == "xsens" then
    xsens_markerfile = scriptdir_path .. "xsens_markers.lua"
    -- Execute the marker description script in the local scope.
    -- This defines the variable "markers_xsens" and assigns a marker table to it.
    dofile(xsens_markerfile)
    markers = markers_xsens
elseif marker_type == "qualisys" then
    -- Do the same for qualisys markers, defining "markers_qualisys"
    qualisys_markerfile = scriptdir_path .. "qualisys_markers.lua"
    dofile(qualisys_markerfile)
    markers = markers_qualisys
elseif marker_type == "h36m" then
    -- Do the same for h36m markers, defining "markers_h36m"
    h36m_markerfile = scriptdir_path .. "h36m_markers.lua"
    dofile(h36m_markerfile)
    markers = markers_h36m
elseif marker_type == "all" then
    -- TODO: Not yet well tested. Use at own risk.
    -- All markers at the same time. Can cause problems in Puppeteer sometimes.
    all_markerfile = scriptdir_path .. "all_markers.lua"
    dofile(all_markerfile)
    markers = markers_all
end



for k, v in pairs(model.frames) do
    -- print(k, v.name)
    model.frames[k].markers = markers[v.name]
end

-- optional: convert it to text so that you can inspect it manually
-- out = serialize( model )
-- print (out)

-- very important: return the created model
return model
