#!/bin/bash

# Open Puppeteer with the multi-markerset HeiMan model using the specified
# markerset type and a .c3d file path that contains recordings of that
# markerset.
#
# Usage: $ bash ph.sh <MARKER_TYPE> <C3D_PATH>
#
#    <MARKER_TYPE> can be qualisys, xsens or h36m

set -euf -o pipefail

export MARKER_TYPE=$1
C3D_PATH=$2

# Directory that contains this bash script file
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

puppeteer "${DIR}/model/model.lua" $C3D_PATH
