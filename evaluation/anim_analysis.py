"""Code for evaluation of exported model animation csvs from Puppeteer"""

# General TODO:
# - Support multiple recordings
# - CLI args to generate specific figures

import enum
import os
from typing import *
import pprint

import numpy as np
import scipy.interpolate
import scipy.ndimage
import matplotlib
import matplotlib.pyplot as plt
import transforms3d as tf
from matplotlib.patches import Polygon
from sklearn.linear_model import LinearRegression

import argparse

expname_choices = [
    'goofy_walk', 'normal_walk', 'slackline', 'squats'
]

parser = argparse.ArgumentParser()
parser.add_argument('expname', help='Name of the experiment', choices=expname_choices)
parser.add_argument('-o', '--outpath', default='/tmp/evamoplots', help='Where to write files')
parser.add_argument('-e', '--fnext', default='pdf', help='Plot file extension')
args = parser.parse_args()

# EXPERIMENT_NAME = 'goofy_walk'
EXPERIMENT_NAME = args.expname

outpath = f'{args.outpath}/{args.expname}'
# PLOTTING_OUT_PATH = '/tmp/evamo'
fnext = args.fnext
plot_dpi = 200

os.makedirs(os.path.expanduser(outpath), exist_ok=True)

# General convention: Time series are ndarrays with axis order (t, d) where t
# is time (in seconds) an d the index of the series (generally the dof
# (degree of freedom of the model)



# Frame rate to which to resample in general. This is the frame rate of QTM capture data.
REF_FRAMERATE: float = 150.0

COLORS = {'Qualisys': 'blue', 'Xsens': 'orange', 'NN': 'green'}


# Ordered name list of degrees of freedom in the HeiMan model
# Based on HeiMan.lua:132 (local joints = ...)
tra_xyz = ['tx', 'ty', 'tz']
rot_yxz = ['ry', 'rx', 'rz']
rot_y = ['ry']
rot_yx = ['ry', 'rx']
rot_z = ['rz']
none = []

DOFNAMES = [
    'Time',
    *(f'Pelvis: {d}' for d in tra_xyz + rot_yxz),

    *(f'HipRight: {d}' for d in rot_yxz),
    *(f'KneeRight: {d}' for d in rot_y),
    *(f'AnkleRight: {d}' for d in rot_yx),

    *(f'HipLeft: {d}' for d in rot_yxz),
    *(f'KneeLeft: {d}' for d in rot_y),
    *(f'AnkleLeft: {d}' for d in rot_yx),

    *(f'Lumbar: {d}' for d in rot_yx),
    *(f'Thorax: {d}' for d in rot_z),
    *(f'ClaviculaRight: {d}' for d in none),
    *(f'ShoulderRight: {d}' for d in rot_yxz),
    *(f'ElbowRight: {d}' for d in rot_y),
    *(f'WristRight: {d}' for d in none),

    *(f'ClaviculaLeft: {d}' for d in none),
    *(f'ShoulderLeft: {d}' for d in rot_yxz),
    *(f'ElbowLeft: {d}' for d in rot_y),
    *(f'WristLeft: {d}' for d in none),
    
    *(f'Neck: {d}' for d in none),
    *(f'Head: {d}' for d in rot_yxz),
]

# Ordered list of names of segments that have at least one DOF (same order as DOFNAMES)
SEGNAMES = list(dict.fromkeys([n.split(':')[0] for n in DOFNAMES]))[1:]

# DOF sequences that represent euler angles (for later euler -> mat -> euler transform)
# Important: This must be kept synchronized with DOFNAMES
EULER_ANGLE_DOFSEQS = {
    (4, 5, 6): 'yxz',  # Pelvis
    (7, 8, 9): 'yxz',  # HipRight
    (10,): 'y',  # KneeRight
    (11, 12): 'yx',  # AnkleRight
    (13, 14, 15): 'yxz',  # HipLeft
    (16,): 'y',  # KneeLeft
    (17, 18): 'yx',  # AnkleLeft
    (19, 20): 'yx',  # Lumbar
    (21,): 'z',  # Thorax
    # (): '',  # ClaviculaRight
    (22, 23, 24): 'yxz',  # ShoulderRight
    (25,): 'y',  # ElbowRight
    # (): '',  # WristRight
    # (): '',  # ClaviculaLeft
    (26, 27, 28): 'yxz',  # ShoulderLeft
    (29,): 'y',  # ElbowLeft
    # (): '',  # WristLeft
    # (): '',  # Neck
    (30, 31, 32): 'yxz'  # Head
}

# DOFNAMES = [f'({i}) {name}' for i, name in enumerate(DOFNAMES)]

def latexify(fig_width=None, fig_height=None):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    fig_width_pt = 397.0   # Get this from LaTeX using \the\textwidth
    inches_per_pt = 1.0/72.27  # Convert pt to inch

    if fig_width is None:
        fig_width = fig_width_pt*inches_per_pt

    if fig_height is None:
        golden_mean = (np.sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    MAX_HEIGHT_INCHES = 8.0
    if fig_height > MAX_HEIGHT_INCHES:
        print("WARNING: fig_height too large:" + fig_height + 
              "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
        fig_height = MAX_HEIGHT_INCHES

    params = {
      'backend': 'pgf',
      'pgf.texsystem': 'lualatex',
      #'text.latex.preamble': ['\usepackage{gensymb}'],
    #   'axes.labelsize': 10, # fontsize for x and y labels (was 10)
    #   'axes.titlesize': 10,
    #   'font.size':       10, # was 10
    #   'legend.fontsize': 10, # was 10
      'xtick.labelsize': 10,
      'ytick.labelsize': 10,
      'text.usetex': True,
      'figure.figsize': [fig_width, fig_height],
      'font.family': 'serif'
    }

    matplotlib.rcParams.update(params)


# latexify(fig_width=5.5)
latexify()


def resample1d(values: np.ndarray, scale: float = 1.0) -> np.ndarray:
    if scale == 1:
        return values

    def __resample1d(values):
        xs = np.arange(0, len(values))
        ys = values
        fi = scipy.interpolate.interp1d(xs, ys, kind='cubic', fill_value='extrapolate')
        nxs = np.arange(0, len(values), 1 / scale)
        nys = fi(nxs)
        return nys

    if values.ndim == 1:
        return __resample1d(values)
    elif values.ndim == 2:
        return np.stack([__resample1d(values[:, j]) for j in range(values.shape[1])], axis=1)
    else:
        raise ValueError


# def rmse(a: np.ndarray, b: np.ndarray) -> Union[float, np.ndarray]:
#     assert a.shape == b.shape
#     rmse = np.sqrt(np.mean((a - b) ** 2, axis=0))
#     return rmse
#
# def mpjpe(a: np.ndarray, b: np.ndarray) -> float:
#     # See Human3.6M paper
#     raise NotImplementedError('TODO: Mean Per Joint Position Error')
#
# def mpjae(a: np.ndarray, b: np.ndarray) -> float:
#     """Mean Per Joint Angle Error"""
#     # See Human3.6M paper
#     assert a.shape == b.shape


# TODO
# def synchronize_dimension(data, rectype):
#     return data
#     # TODO: Synchronize coordinate systems
#     if rectype == 'Qualisys':
#         return data
#     elif rectype == 'Xsens':
#         # tmp = data[:, 1]
#         data = synchronize_xsens_rot(data, xsens)
#         return data
#     elif rectype == 'NN':
#         tmp = data[:, 1]
#         return data
#     else:
#         raise ValueError(f'Invalid rectype {rectype}')

def rotatefbz(data, angle):
    """Rotate floating base around z axis by `angle` for global coordinate
    frame synchronization.
    Note the different axis orders: xyz vs. yxz."""
    dt = data.T  # transpose to (DOF, t) shape for easier handling

    fbtrans = dt[1:4]  # xyz translation of floating base (pelvis)
    fbrot_euler = dt[4:7]  # yxz rotation of floating base (pelvis)

    # Apply rotation to fb translation data
    rotmat_xyz = tf.euler.euler2mat(0, 0, angle, 'sxyz')
    rotated_fbtrans = rotmat_xyz @ fbtrans

    # Apply rotation to fb rotation data. This only affects the z rotation.
    rotated_fbrot_euler = fbrot_euler.copy()    
    rotmat_yxz = tf.euler.euler2mat(0, 0, angle, 'syxz')
    for t in range(dt.shape[1]): # For all time steps
        # First, convert original fb euler angle data to rotation matrix
        fbrotmat = tf.euler.euler2mat(fbrot_euler[0,t], fbrot_euler[1,t], fbrot_euler[2,t], 'syxz')
        # Then we can apply the desired additional rotation to the fb rotation data
        rotated_fbrotmat = rotmat_yxz @ fbrotmat
        rotated_fbrot_euler[:, t] = tf.euler.mat2euler(rotated_fbrotmat, 'syxz')
        # rotated_fbrot_euler[3, t] -= 2 * np.pi

    # if unwrap_mult:
    # Fix z rotation ambiguity caused by periodicity of rotation angles.
    # rotated_fbrot_euler[2, :] = np.unwrap(rotated_fbrot_euler[2, :]) + (unwrap_mult * 2 * np.pi)

    rotated = dt.copy()  # Original transposed data
    # Overwrite floating base data by rotated versions
    rotated[1:4] = rotated_fbtrans
    rotated[4:7] = rotated_fbrot_euler
    rotated = rotated.T  # transpose back to (t, DOF)
    return rotated


def reeulerize(data):
    """Convert DOF sequences that represent euler angles of the same segment
    jointly to rotation matrix form and then back to euler angles in order
    to normalize otherwise arbitrary choices in euler angle parametrizations"""
    dt = data.copy().T  # transpose to (DOF, t) shape for easier handling

    for idseq, axseq in EULER_ANGLE_DOFSEQS.items():
        axes = 's' + axseq
        idslc = slice(idseq[0], idseq[-1] + 1)

        if axseq == 'yxz':
            y, x, z = dt[idslc]
        elif axseq == 'yx':
            y, x = dt[idslc]
            z = np.zeros(dt.shape[1])
        elif axseq == 'y':
            y = dt[idslc][0]
            x = np.zeros(dt.shape[1])
            z = np.zeros(dt.shape[1])
        elif axseq == 'z':
            y = np.zeros(dt.shape[1])
            x = np.zeros(dt.shape[1])
            z = dt[idslc][0]

        for t in range(dt.shape[1]):
            mat = tf.euler.euler2mat(y[t], x[t], z[t], 'syxz')
            eul = tf.euler.mat2euler(mat, 'syxz')

            if axseq == 'yxz':
                pass  # yxz is fine
            elif axseq == 'yx':
                eul = eul[:2]  # Only yx, discard z
            elif axseq == 'y':
                eul = eul[0]  # Only y, discard x and z
            elif axseq == 'z':
                eul = eul[2]  # Only z, discard y and x

            dt[idslc, t] = eul
    # dt = np.unwrap(dt)
    return dt.T


def zero_origin(x):
    """Shift fb translations to begin at the origin (0, 0, 0)"""
    y = np.empty_like(x)
    for i in range(x.shape[0]):
        y[i] = x[i] - x[i, 0]
    return y

def prepare_recording(
        csv_path: str,  # Path to csv containing joint angle exports
        framerate: float,  # Frame rate of the recording
        ref_offset_frame: int,  # Reference frame offset (in original recorded frames)
        pre_ref_offset_secs: float,
        post_ref_offset_secs: float,
        rectype: str,
        rotsync_angle: float = 0.0,  # Rotate around z axis by this angle,
        unwrap_mult: int = 0  # Fix z rotation ambiguity. Only used if rotsync_angle != 0
) -> np.ndarray:

    # -- 1. Load raw data
    raw_data = np.loadtxt(csv_path, delimiter=',')

    # -- 2. Resample to reference frame rate
    # Frame rate scale (ratio for scaling to reference frame rate)
    frscale = REF_FRAMERATE / framerate
    resampled_data = resample1d(raw_data, scale=frscale)

    # -- 3. Find begin and end frame where to cut, aligned to reference frame
    # Reference frame offset in resampled data
    ref_frame_offset = ref_offset_frame * frscale
    # Number of frames in resampled data that should be kept before the reference frame
    # (i.e. temporal distance between reference frame and beginning of evaluatable recording)
    pre_ref_offset_frames = pre_ref_offset_secs * REF_FRAMERATE
    # Number of frames to keep after the reference frame
    post_ref_offset_frames = post_ref_offset_secs * REF_FRAMERATE
    begin = round(ref_frame_offset - pre_ref_offset_frames)
    end = round(ref_frame_offset + post_ref_offset_frames)

    # -- 4. Cut recording to the desired size and align the starting point
    offset_data = resampled_data[begin:end]
    # Recalculate time information
    offset_data[:, 0] = np.linspace(0, offset_data.shape[0] / REF_FRAMERATE, offset_data.shape[0])

    # -- 5. Shift start position of floating base to origin (0, 0, 0), because the start point is arbitrary
    shifted_data = offset_data.copy()
    initial_pos = offset_data[0, 1:4]
    shifted_data[:, 1:4] = offset_data[:, 1:4] - initial_pos

    # -- 5. Apply optimal rotation around z axis to synchronize coordinate system with Qualisys system
    if rotsync_angle != 0.0:
        rot_data = rotatefbz(shifted_data, rotsync_angle)
    else:
        rot_data = shifted_data

    # -- 6. Re-eulerize, unwrap discontinuities
    rot_data = reeulerize(rot_data)
    # Fix ambiguity of manipulated z rotation of Pelvis caused by periodicity of rotation angles (see step 5.)
    rot_data.T[6] = np.unwrap(rot_data.T[6]) + (unwrap_mult * 2 * np.pi)
    
    return rot_data


def rotmat_angle_error(R1, R2):
    R = R1.T @ R2
    l = np.array([R[2,1] - R[1,2], R[0,2] - R[2,0] , R[1,0] - R[0,1]])
    ang = np.arctan2(np.linalg.norm(l),( R[0,0] + R[1,1] + R[2,2] - 1.0))  
    return ang

def calc_angle_errors(recordings, target_key='Qualisys'):
    """Calculate angle errors"""
    aedict = {}
    target_rec = recordings[target_key]
    tart = target_rec.T  # transpose to (segs, t) shape for easier handling
    nframes = tart.shape[1]
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself

        rect = rec.T  # transpose to (segs, t) shape for easier handling

        angle_errors = np.zeros((len(EULER_ANGLE_DOFSEQS), nframes))

        for seg_idx, (idseq, axseq) in enumerate(EULER_ANGLE_DOFSEQS.items()):
            axes = 's' + axseq
            idslc = slice(idseq[0], idseq[-1] + 1)

            if axseq == 'yxz':
                ry, rx, rz = rect[idslc]
                ty, tx, tz = tart[idslc]
            elif axseq == 'yx':
                ry, rx = rect[idslc]
                ty, tx = tart[idslc]
                rz = np.zeros(nframes)
                tz = np.zeros(nframes)
            elif axseq == 'y':
                ry = rect[idslc][0]
                ty = tart[idslc][0]
                rx = np.zeros(nframes)
                tx = np.zeros(nframes)
                rz = np.zeros(nframes)
                tz = np.zeros(nframes)
            elif axseq == 'z':
                ry = np.zeros(nframes)
                ty = np.zeros(nframes)
                rx = np.zeros(nframes)
                tx = np.zeros(nframes)
                rz = rect[idslc][0]
                tz = tart[idslc][0]

            for t in range(nframes):
                rmat = tf.euler.euler2mat(ry[t], rx[t], rz[t], 'syxz')
                tmat = tf.euler.euler2mat(ty[t], tx[t], tz[t], 'syxz')
                angle_errors[seg_idx, t] = rotmat_angle_error(rmat, tmat)
        aedict[rec_name] = (
            rec[:, 0],  # time stamps
            angle_errors,  # errors per time step
        )
    return aedict


def get_mean_angle_error(aedict):
    maedict = {}
    for rec_name, (time, errors) in aedict.items():
        maedict[rec_name] = np.mean(errors)
    return maedict


def plot_dofs(recordings, dof_idx, title_prefix=None, ax=None):
    pi2 = 2 * np.pi
    for rec_name, rec in recordings.items():
        x = rec[:, 0]
        y = rec[:, dof_idx]
        ax.plot(x, y, label=rec_name, color=COLORS[rec_name], linewidth=0.8)

    dofname = DOFNAMES[dof_idx]
    # ax.set_title(f'({title_prefix}) {dofname}' if title_prefix is not None else dofname)
    ax.set_title(f'{title_prefix}  |  {dofname}' if title_prefix is not None else dofname)    
    ax.set_xlim(xmin=np.min(x), xmax=np.max(x))
    # ax.xaxis.set_ticks(np.arange(np.min(x), np.max(x), 1.0))
    ax.minorticks_on()
    ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
    ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')


def plot_dof_correlation(recordings, dof_idx, target_key='Qualisys', title_prefix=None, ax=None):
    ax.set_rasterized(True)
    target_rec = recordings[target_key]
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself
        a = rec[:, dof_idx]
        b = target_rec[:, dof_idx]
        ax.scatter(a, b, label=rec_name, color=COLORS[rec_name])

    dofname = DOFNAMES[dof_idx]
    # ax.set_title(f'({title_prefix}) {dofname}' if title_prefix is not None else dofname)
    ax.set_title(f'{title_prefix}  |  {dofname}' if title_prefix is not None else dofname)    
    # ax.xaxis.set_ticks(np.arange(np.min(x), np.max(x), 1.0))
    ax.minorticks_on()
    ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
    ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')


def plot_metric(recordings, metric_fn, dof_idx, target_key='Qualisys', title_prefix=None, ax=None):
    target_rec = recordings[target_key]
    x = target_rec[:, 0]
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself
        if dof_idx is None:  # Don't select a joint, evaluate metric on all joints
            a = rec
            b = target_rec
        else:
            a = rec[:, dof_idx]
            b = target_rec[:, dof_idx]
        y = metric_fn(a, b)
        ax.plot(x, y, label=rec_name, color=COLORS[rec_name], linewidth=0.8)
    
    dofname = DOFNAMES[dof_idx]
    # ax.set_title(f'({title_prefix}) {dofname}' if title_prefix is not None else dofname)
    ax.set_title(f'{title_prefix}  |  {dofname}' if title_prefix is not None else dofname)    
    ax.set_xlim(xmin=np.min(x), xmax=np.max(x))
    # ax.set_ylim(-2 * np.pi, 2 * np.pi)
    # ax.xaxis.set_ticks(np.arange(np.min(x), np.max(x), 1.0))
    ax.minorticks_on()
    ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
    ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')


def get_timemedians_of_metric(recordings, metric_fn, dof_inds, target_key='Qualisys'):
    target_rec = recordings[target_key]
    medians = {}
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself
        medians[rec_name] = {}
        for dof_idx in dof_inds:
            a = rec[:, dof_idx]
            b = target_rec[:, dof_idx]
            y = metric_fn(a, b)
            med = np.median(y)
            medians[rec_name][DOFNAMES[dof_idx]] = med
    return medians


def get_mean_of_metric(recordings, metric_fn, dof_inds, target_key='Qualisys'):
    target_rec = recordings[target_key]
    means = {}
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself
        means_ = []
        for dof_idx in dof_inds:
            a = rec[:, dof_idx]
            b = target_rec[:, dof_idx]
            y = metric_fn(a, b)
            means_.append(np.mean(y))
        means[rec_name] = np.mean(means_)
    return means

def plot_dofmeans_of_metric(recordings, metric_fn, dof_inds, target_key='Qualisys', ax=None):
    target_rec = recordings[target_key]
    x = target_rec[:, 0]
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself
        a = rec[:, dof_inds]
        b = target_rec[:, dof_inds]
        y = metric_fn(a, b)
        m = np.mean(y, axis=1)  # Preserve temporal dimension 0
        ax.plot(x, m, label=rec_name, color=COLORS[rec_name], linewidth=0.8)

    ax.set_xlim(xmin=np.min(x), xmax=np.max(x))
    # ax.xaxis.set_ticks(np.arange(np.min(x), np.max(x), 1.0))
    ax.minorticks_on()
    ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
    ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')
    # ax.legend()

    # return dofmeans


# TODO: Highly redundant with plot_dofmeans_of_metric()
def find_linreg_coeffs(recordings, metric_fn, dof_inds, target_key='Qualisys'):
    """Find slope of linear regression model fit to mean of dof metrics.
    The idea is to find out if errors grow over time (<- positive slope) and
    especially if we can measure error propagation in Xsens recordings.
    """
    target_rec = recordings[target_key]
    x = target_rec[:, 0]
    slopes = {}
    for rec_name, rec in recordings.items():
        if rec_name == target_key:
            continue  # Don't evaluate target recording against itself
        a = rec[:, dof_inds]
        b = target_rec[:, dof_inds]
        y = metric_fn(a, b)
        m = np.mean(y, axis=1)  # Preserve temporal dimension 0
        # dofmeans[rec_name] = m
        reg = LinearRegression().fit(x.reshape(-1, 1), m)
        slope = reg.coef_[0]
        slopes[rec_name] = slope
    return slopes

def find_linreg_coeffs_of_mean_angle_errors(recordings):
    aedict = calc_angle_errors(recordings)
    slopes = {}

    for rec_name, (time, errors) in aedict.items():
        m = np.mean(errors, axis=0)
        reg = LinearRegression().fit(time.reshape(-1, 1), m)
        slope = reg.coef_[0]
        slopes[rec_name] = slope

    return slopes


def plot_box(recordings, metric_fn, dof_inds, target_key='Qualisys', ax=None):
    target_rec = recordings[target_key]
    ys = []
    labels = []
    for dof_idx in dof_inds:  # TODO
        for rec_name, rec in recordings.items():
            if rec_name == target_key:
                continue  # Don't evaluate target recording against itself
            a = rec[:, dof_idx]
            b = target_rec[:, dof_idx]
            y = metric_fn(a, b)
            ys.append(y)
            # labels.append(f'{rec_name[:1]}{dof_idx}')
            labels.append(f'DOF {dof_idx}  |  {DOFNAMES[dof_idx]}')

    box_colors = ['orange', 'green']

    # Reverse everything so boxes are ordered top to bottom
    ys = ys[::-1]
    labels = labels[::-1]
    box_colors = box_colors[::-1]

    ax.xaxis.grid(True, linestyle='-', which='major', color='grey', alpha=0.5)
    bp = ax.boxplot(ys, labels=labels, notch=False, vert=False)

    # Set one shared tick for xsens and nn
    oldticks = ax.get_yticks()
    newticks = oldticks[:-1:2] + 0.5
    ax.set_yticks(newticks)
    ax.set_yticklabels(labels[::2])#, rotation=45)
    ax.minorticks_on()
    hlines = oldticks[1:-1:2] + 0.5
    ax.hlines(hlines, 0, ax.get_xlim()[1], linestyles='dashed', colors='grey')
    ax.margins(0)

    # Colorize according to category (xsens vs nn)
    for i in range(len(dof_inds) * 2):
        bp['medians'][i].set_color('black')
        box = bp['boxes'][i]
        boxX = []
        boxY = []
        for j in range(5):  # Assumes notch=False, looks weird with notch=True
            boxX.append(box.get_xdata()[j])
            boxY.append(box.get_ydata()[j])
        box_coords = np.column_stack([boxX, boxY])
        ax.add_patch(Polygon(box_coords, facecolor=box_colors[i % 2]))


def plot_ungrouped_box(recordings, metric_fn, dof_inds, target_key='Qualisys', ax=None):
    target_rec = recordings[target_key]
    ys = []
    labels = []
    for dof_idx in dof_inds:  # TODO
        for rec_name, rec in recordings.items():
            if rec_name == target_key:
                continue  # Don't evaluate target recording against itself
            a = rec[:, dof_idx]
            b = target_rec[:, dof_idx]
            y = metric_fn(a, b)
            ys.append(y)
            # labels.append(f'{rec_name[:1]}{dof_idx}')
            labels.append(f'DOF {dof_idx}  |  {DOFNAMES[dof_idx]}')

    box_colors = ['orange']

    # Reverse everything so boxes are ordered top to bottom
    ys = ys[::-1]
    labels = labels[::-1]
    box_colors = box_colors[::-1]

    ax.xaxis.grid(True, linestyle='-', which='major', color='grey', alpha=0.5)
    bp = ax.boxplot(ys, labels=labels, notch=False, vert=False)

    # Set one shared tick for xsens and nn
    # oldticks = ax.get_yticks()
    # newticks = oldticks[:-1:2] + 0.5
    # ax.set_yticks(newticks)
    ax.set_yticklabels(labels)
    ax.minorticks_on()
    hlines = ax.get_yticks()[:-1] + 0.5
    ax.hlines(hlines, 0, ax.get_xlim()[1], linestyles='dashed', colors='grey')
    ax.margins(0)

    # Colorize according to category (xsens vs nn)
    for i in range(len(dof_inds)):
        bp['medians'][i].set_color('black')
        box = bp['boxes'][i]
        boxX = []
        boxY = []
        for j in range(5):  # Assumes notch=False, looks weird with notch=True
            boxX.append(box.get_xdata()[j])
            boxY.append(box.get_ydata()[j])
        box_coords = np.column_stack([boxX, boxY])
        ax.add_patch(Polygon(box_coords, facecolor=box_colors[0]))


def signed_difference(pred, targ):
    return targ - pred


def abs_difference(pred, targ):
    return np.abs(targ - pred)


def mse(pred, targ):
    return (pred - targ) ** 2


def rmse(pred, targ):
    return np.sqrt(mse(pred, targ))


def mpjae(pred, targ):
    # ns = 10 # Number of joints in skeleton
    # Not using origingal h36m paper formula because
    # we don't have as many degrees of freedom
    n = targ.shape[0]
    pjae = np.abs(pred - targ)  # TODO: modulo 180 degrees
    return np.mean(pjae, axis=1)


def time_mean(metric_fn):
    def mean_metric_fn(*args, **kwargs):
        return np.mean(metric_fn(*args, **kwargs), axis=0)
    return mean_metric_fn

num_dofs = len(DOFNAMES) - 1  # Without time


def get_euclideandistance(xrec, qrec, mean=False):
    xcoords = xrec[:, 1:4]
    qcoords = qrec[:, 1:4]
    dists = np.linalg.norm(xcoords - qcoords, axis=1)
    if mean:
        return np.mean(dists)
    else:
        return dists


def multiplot_angle_errors():
    fig, axes = plt.subplots(7, 2, figsize=(9, 12))
    # Delete unused axes
    # fig.delaxes(axes[-1, -2])
    # fig.delaxes(axes[-1, -1])

    aedict = calc_angle_errors(recordings)

    for i, segname in enumerate(SEGNAMES):
        for rec_name, (time, errors) in aedict.items():
            ax = axes.flat[i]
            ax.plot(time, errors[i], color=COLORS[rec_name], linewidth=0.8)
            ax.set_title(segname)
            ax.set_xlim(xmin=np.min(time), xmax=np.max(time))
            # ax.xaxis.set_ticks(np.arange(np.min(time), np.max(time), 1.0))
            ax.minorticks_on()
            ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
            ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')

    fig.tight_layout()
    fig.savefig(f'{outpath}/angle-errors.{fnext}', dpi=plot_dpi)

    plt.close('all')


def plot_mean_angle_errors():
    fig, ax = plt.subplots(figsize=(9, 3))

    aedict = calc_angle_errors(recordings)

    for rec_name, (time, errors) in aedict.items():
        mean_errors = np.mean(errors, axis=0)
        ax.plot(time, mean_errors, color=COLORS[rec_name], linewidth=0.8)
        # ax.set_title(segname)
        ax.set_xlim(xmin=np.min(time), xmax=np.max(time))
        # ax.xaxis.set_ticks(np.arange(np.min(time), np.max(time), 1.0))
        ax.minorticks_on()
        ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
        ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')

    fig.tight_layout()
    fig.savefig(f'{outpath}/mean-angle-errors.{fnext}', dpi=plot_dpi)

    plt.close('all')


def multiplot_angles():
    # Offset: 1 for time, 3 for translation in x,y,z
    dof_offset = 1 + 3
    dof_inds = range(dof_offset, num_dofs)
    fig, axes = plt.subplots(7,4, figsize=(14, 20))
    # Delete unused axes
    # fig.delaxes(axes.flat[-dof_inds.stop:])
    # fig.delaxes(axes[-1, -2])
    # fig.delaxes(axes[-1, -1])

    for i in dof_inds:
        plot_dofs(recordings, i, ax=axes.flat[i - dof_offset], title_prefix=f'DOF {i}')

    # Shared legend
    handles, labels = axes[0][0].get_legend_handles_labels()
    # fig.legend(handles, labels, loc='lower right')
    
    fig.tight_layout()
    fig.savefig(f'{outpath}/angledof.{fnext}', dpi=plot_dpi)

    plt.close('all')


def multiplot_angle_correlations():
    # Offset: 1 for time, 3 for translation in x,y,z
    dof_offset = 1 + 3
    dof_inds = range(dof_offset, num_dofs)
    fig, axes = plt.subplots(7,4, figsize=(14, 20))
    # fig.delaxes(axes[-1, -2])
    # fig.delaxes(axes[-1, -1])

    for i in dof_inds:
        plot_dof_correlation(recordings, i, ax=axes.flat[i - dof_offset])

    # Shared legend
    handles, labels = axes[0][0].get_legend_handles_labels()
    fig.legend(handles, labels, loc='lower right')
    
    fig.tight_layout()
    # fig.savefig(f'{outpath}/corr_angles.{fnext}', dpi=plot_dpi)

    plt.close('all')


def multiplot_translation():
    # Offset: 1 for time
    dof_offset = 1
    dof_inds = range(dof_offset, dof_offset + 3)
    fig, axes = plt.subplots(1,3, figsize=(9, 3))

    for i in dof_inds:
        plot_dofs(
            recordings_with_fbtranslation,
            i,
            title_prefix=f'DOF {i}',
            ax=axes[i - dof_offset]
        )
    # handles, labels = axes[0].get_legend_handles_labels()
    # fig.legend(handles, labels, loc='right')
    fig.tight_layout()
    fig.savefig(f'{outpath}/translationdof.{fnext}', dpi=plot_dpi)

    plt.close('all')


def multiplot_angle_metric(metric_fn):
    # Offset: 1 for time, 3 for translation in x,y,z
    dof_offset = 1 + 3
    dof_inds = range(dof_offset, num_dofs)
    fig, axes = plt.subplots(7,4, figsize=(14, 20))
    # Delete unused axes
    # fig.delaxes(axes.flat[-angle_dof_inds.stop:])
    # fig.delaxes(axes[-1, -2])
    # fig.delaxes(axes[-1, -1])

    for i in dof_inds:
        plot_metric(
            recordings,
            metric_fn,
            i,
            title_prefix=f'DOF {i}',
            ax=axes.flat[i - dof_offset]
        )

    handles, labels = axes[0][0].get_legend_handles_labels()
    # fig.legend(handles, labels, loc='lower right')
    fig.tight_layout()
    fig.savefig(f'{outpath}/angledof-absdiff.{fnext}', dpi=plot_dpi)

    plt.close('all')


def multiplot_translation_metric(metric_fn):
    # Offset: 1 for time
    dof_offset = 1
    dof_inds = range(dof_offset, dof_offset + 3)
    fig, axes = plt.subplots(1,4, figsize=(9, 2.5))

    for i in dof_inds:
        plot_metric(
            recordings_with_fbtranslation,
            metric_fn,
            i,
            title_prefix=f'DOF {i}',
            ax=axes[i - dof_offset]
        )
    # Plot euclidean distance
    distax = axes[3]
    times = recordings['Qualisys'][:, 0]
    dists = get_euclideandistance(recordings['Xsens'], recordings['Qualisys'])
    distax.plot(times, dists, color=COLORS['Xsens'], linewidth=0.8)
    distax.set_title('Euclidean distance')
    distax.set_xlim(xmin=np.min(times), xmax=np.max(times))
    distax.minorticks_on()
    distax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
    distax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')

    # handles, labels = axes[0][0].get_legend_handles_labels()
    # fig.legend(handles, labels, loc='lower right')
    fig.tight_layout()
    fig.savefig(f'{outpath}/translationdof-absdiff.{fnext}', dpi=plot_dpi)

    plt.close('all')


def plot_angle_diffbox(metric_fn):
    # Offset: 1 for time, 3 for translation in x,y,z
    dof_offset = 1 + 3
    dof_inds = range(dof_offset, num_dofs + 1)
    fig, ax = plt.subplots(figsize=(9, 14))

    plot_box(
        recordings,
        metric_fn,
        dof_inds,
        ax=ax
    )

    fig.tight_layout()
    fig.savefig(f'{outpath}/angledof-absdiff-box.{fnext}', dpi=plot_dpi)

    plt.close('all')


def plot_translation_diffbox(metric_fn):
    # Offset: 1 for time, 3 for translation in x,y,z
    dof_offset = 1
    dof_inds = range(dof_offset, dof_offset + 3)
    fig, ax = plt.subplots(figsize=(9, 1.5))

    plot_ungrouped_box(
        recordings_with_fbtranslation,
        metric_fn,
        dof_inds,
        ax=ax,
    )

    fig.tight_layout()
    fig.savefig(f'{outpath}/translationdof-absdiff-box.{fnext}', dpi=plot_dpi)

    plt.close('all')


def plot_angle_error_box():
    vertical = False
    aedict = calc_angle_errors(recordings)
    if vertical:
        fig, ax = plt.subplots(figsize=(16, 8))
    else:
        fig, ax = plt.subplots(figsize=(9, 8))
    ys = []
    labels = []
    for i, segname in enumerate(SEGNAMES):
        for rec_name, (time, errors) in aedict.items():
            ys.append(errors[i])
            labels.append(SEGNAMES[i])

    box_colors = ['orange', 'green']
    if vertical:  # Enable for vertical bars
        ax.yaxis.grid(True, linestyle='-', which='major', color='grey', alpha=0.5)
        bp = ax.boxplot(ys, labels=labels, notch=False)

        # Set one shared tick for xsens and nn
        oldticks = ax.get_xticks()
        newticks = oldticks[:-1:2] + 0.5
        vlines = oldticks[1:-1:2] + 0.5
        ax.vlines(vlines, 0, ax.get_ylim()[1], linestyles='dashed', colors='grey')
        ax.set_xticks(newticks)
        ax.set_xticklabels(labels[::2], rotation=45)
    else:  # Horizontal bars
        # Reverse everything so boxes are ordered top to bottom
        ys = ys[::-1]
        labels = labels[::-1]
        box_colors = box_colors[::-1]

        ax.xaxis.grid(True, linestyle='-', which='major', color='grey', alpha=0.5)
        bp = ax.boxplot(ys, labels=labels, notch=False, vert=False)

        # Set one shared tick for xsens and nn
        oldticks = ax.get_yticks()
        newticks = oldticks[:-1:2] + 0.5
        hlines = oldticks[1:-1:2] + 0.5
        ax.hlines(hlines, 0, ax.get_xlim()[1], linestyles='dashed', colors='grey')
        ax.set_yticks(newticks)
        ax.set_yticklabels(labels[::2])

    ax.minorticks_on()
    ax.margins(0)

    # Colorize according to category (xsens vs nn)
    for i in range(len(SEGNAMES) * 2):
        bp['medians'][i].set_color('black')
        box = bp['boxes'][i]
        boxX = []
        boxY = []
        for j in range(5):  # Assumes notch=False, looks weird with notch=True
            boxX.append(box.get_xdata()[j])
            boxY.append(box.get_ydata()[j])
        box_coords = np.column_stack([boxX, boxY])
        ax.add_patch(Polygon(box_coords, facecolor=box_colors[i % 2]))
    
    fig.tight_layout()
    fig.savefig(f'{outpath}/angle-error-box.{fnext}', dpi=plot_dpi)

    plt.close('all')


def plot_dofmeans_angles():
    # Offset: 1 for time, 3 for translation in x,y,z
    dof_offset = 1 + 3
    dof_inds = range(dof_offset, num_dofs + 1)
    fig, ax = plt.subplots(figsize=(9, 3))
    plot_dofmeans_of_metric(recordings, abs_difference, dof_inds, ax=ax)

    fig.tight_layout()
    fig.savefig(f'{outpath}/mean-angledof-absdiff.{fnext}', dpi=plot_dpi)


def plot_translation_trajectory():
    # Offset: 1 for time
    fig, ax = plt.subplots(figsize=(9, 6))

    for rec_name, rec in recordings.items():
        if rec_name == 'NN':
            continue  # NN has no valid fb translation predictions
        xs, ys = rec.T[1], rec.T[2]
        ax.plot(xs, ys, color=COLORS[rec_name], linewidth=0.8)
        ax.set_xlabel('x (m)')
        ax.set_ylabel('y (m)')
        # ax.set_xlim(xmin=-5, xmax=5)
        # ax.set_ylim(ymin=-2, ymax=2)
        # ax.margins(0.1)
        # ax.minorticks_on()
        ax.grid(which='major', linestyle='-', linewidth='0.2', color='black')
        ax.grid(which='minor', linestyle=':', linewidth='0.2', color='grey')
        ax.set_aspect('equal', 'datalim')

    fig.tight_layout()
    fig.savefig(f'{outpath}/trajectory.{fnext}', dpi=plot_dpi)

    plt.close('all')


def write_scalar_metrics(metric_fn):
    translation_time_median_metrics = get_timemedians_of_metric(recordings, metric_fn, range(1, 4))
    angle_time_median_metrics = get_timemedians_of_metric(recordings, metric_fn, range(4, num_dofs + 1))
    translation_linregcoeff = find_linreg_coeffs(recordings, abs_difference, range(1, 4))
    angle_linregcoeff = find_linreg_coeffs_of_mean_angle_errors(recordings)

    mean_angle_absdiff = get_mean_of_metric(recordings, metric_fn, range(4, num_dofs + 1))
    mean_angle_error = get_mean_angle_error(calc_angle_errors(recordings))

    med = get_euclideandistance(recordings['Xsens'], recordings['Qualisys'], mean=True)

    with open(f'{outpath}/scalar-metrics.txt', 'w') as f:
        f.write('translation_time_median_metrics = \\\n')
        f.write(pprint.pformat(translation_time_median_metrics, indent=4, width=100, compact=False))
        f.write('\n\nangle_time_median_metrics = \\\n')
        f.write(pprint.pformat(angle_time_median_metrics, indent=4, width=100, compact=False))
        # f.write(f'\n\nxsens_angle_mean_metric = {np.mean(list(angle_time_median_metrics["Xsens"].values()))}')
        f.write(f'\n\nmean_angle_absdiff = \\\n')
        f.write(pprint.pformat(mean_angle_absdiff, indent=4, width=100, compact=False))
        f.write(f'\n\nmean_angle_error = \\\n')
        f.write(pprint.pformat(mean_angle_error, indent=4, width=100, compact=False))
        f.write('\n\ntranslation_linregcoeff = ')
        f.write(repr(translation_linregcoeff))
        f.write('\n\nangle_linregcoeff = ')
        f.write(repr(angle_linregcoeff))
        f.write('\n\nx_vs_q_mean_euclideandistance = ')
        f.write(repr(med))


if EXPERIMENT_NAME == 'goofy_walk':
    qpath = '../../goofy_walk/eval/qualisys/animation.csv'
    xpath = '../../goofy_walk/eval/xsens/animation.csv'
    hpath = '../../goofy_walk/eval/nn/animation.csv'

    # All other variables are derived from manual recording analysis:

    # Framerates found from c3d headers or product specs
    framerates = {'Qualisys': 150.0, 'Xsens': 60.0, 'NN': 25.0}
    # Manually found offset by looking at the elbow joint angle curve,
    # e.g. `plot(h[:, 31])`, and identifying the timestamp at which the
    # elbow joint is completely bent for the first time
    # (i.e. local minimum joint angle in the plot).
    time_offset = {'Qualisys': 165, 'Xsens': 68, 'NN': 78}

    pre_ref_offset_secs = 1.0
    post_ref_offset_secs = 10.5

    xrotsync_angle = 1.6336
    xunwrap_mult = 0

    hrotsync_angle = 4.2
    hunwrap_mult = -1

elif EXPERIMENT_NAME == 'normal_walk':
    qpath = '../../normal_walk/eval/qualisys/animation.csv'
    xpath = '../../normal_walk/eval/xsens/animation.csv'
    hpath = '../../normal_walk/eval/nn/animation.csv'

    # Framerates found from c3d headers or product specs
    framerates = {'Qualisys': 150.0, 'Xsens': 60.0, 'NN': 30.0}
    # First maximum bend of elbow joint
    time_offset = {'Qualisys': 365, 'Xsens': 147, 'NN': 76}

    pre_ref_offset_secs = 2.0
    post_ref_offset_secs = 11.2

    xrotsync_angle = 3.00
    xunwrap_mult = 0

    hrotsync_angle = 4.4
    hunwrap_mult = 0

elif EXPERIMENT_NAME == '_____old_squats':  # This one was broken unfortunately
    qpath = '../../Squat/eval/qualisys/animation.csv'
    xpath = '../../Squat/eval/xsens/animation.csv'
    hpath = '../../Squat/eval/nn/animation.csv'

    # Framerates found from c3d headers or product specs
    framerates = {'Qualisys': 150.0, 'Xsens': 60.0, 'NN': 25.0}
    # First maximum bend of both knee joints
    time_offset = {'Qualisys': 524, 'Xsens': 178, 'NN': 227}

    pre_ref_offset_secs = 2.8
    post_ref_offset_secs = 15

    xrotsync_angle = 0.125
    xunwrap_mult = 0

    hrotsync_angle = 3.0
    hunwrap_mult = 0

elif EXPERIMENT_NAME == 'squats':
    qpath = '../../Squat_Front/eval/qualisys/animation.csv'
    xpath = '../../Squat_Front/eval/xsens/animation.csv'
    hpath = '../../Squat_Front/eval/nn/animation.csv'

    # Framerates found from c3d headers or product specs
    framerates = {'Qualisys': 150.0, 'Xsens': 60.0, 'NN': 30.0}
    # First maximum bend of both knee joints
    time_offset = {'Qualisys': 516, 'Xsens': 205, 'NN': 226}

    pre_ref_offset_secs = 2.0
    post_ref_offset_secs = 21.2

    xrotsync_angle = 1.72
    xunwrap_mult = 0

    hrotsync_angle = 3.0
    hunwrap_mult = 0

elif EXPERIMENT_NAME == 'slackline':
    qpath = '../../Slackline_front/eval/qualisys/animation.csv'
    xpath = '../../Slackline_front/eval/xsens/animation.csv'
    hpath = '../../Slackline_front/eval/nn/animation.csv'
    
    # Framerates found from c3d headers or product specs
    framerates = {'Qualisys': 150.0, 'Xsens': 60.0, 'NN': 30.0}
    # First maximum bend of both knee joints
    time_offset = {'Qualisys': 456, 'Xsens': 154, 'NN': 101}

    pre_ref_offset_secs = 2.5
    post_ref_offset_secs = 30.0

    xrotsync_angle = 1.55 # 1.63
    xunwrap_mult = 0

    hrotsync_angle = 3.12
    hunwrap_mult = 0

else:
    raise ValueError('Specify experiment name')


q = prepare_recording(
    qpath,
    framerate=framerates['Qualisys'],
    ref_offset_frame=time_offset['Qualisys'],
    pre_ref_offset_secs=pre_ref_offset_secs,
    post_ref_offset_secs=post_ref_offset_secs,
    rectype='Qualisys'
)
x = prepare_recording(
    xpath,
    framerate=framerates['Xsens'],
    ref_offset_frame=time_offset['Xsens'],
    pre_ref_offset_secs=pre_ref_offset_secs,
    post_ref_offset_secs=post_ref_offset_secs,
    rectype='Xsens',
    rotsync_angle=xrotsync_angle,
    unwrap_mult=xunwrap_mult
)
h = prepare_recording(
    hpath,
    framerate=framerates['NN'],
    ref_offset_frame=time_offset['NN'],
    pre_ref_offset_secs=pre_ref_offset_secs,
    post_ref_offset_secs=post_ref_offset_secs,
    rectype='NN',
    rotsync_angle=hrotsync_angle,
    unwrap_mult=hunwrap_mult
)

# Contains the non-goldstandard-method-recordings, i.e. Xsens and NNs
recordings = {'Qualisys': q, 'Xsens': x, 'NN': h}
recordings_to_evaluate = {'Xsens': x, 'NN': h}
recordings_with_fbtranslation = {'Qualisys': q, 'Xsens': x}


if __name__ == '__main__':

    # TODO: Update descriptions for plots_v9

    print("""
    Global color coding of all plots:
     blue: Qualisys
     orange: Xsens
     green: Neural networks
    """)

    print("""
    (angledof.pdf) All 28 angle DOF values over time.
     x axis: time (s)
     y axis: angle (rad)
    """)
    multiplot_angles()

    print("""
    (angledof-absdiff.pdf) Absolute angle DOF differences over time between
     - orange: Qualisys and NN
     - green: Qualisys and Xsens
     x axis: time (s)
     y axis: absolute angle differences (rad)
    """)
    multiplot_angle_metric(rmse)

    print("""
    (angledof-absdiff-box.pdf) Box plots of DOF differences over time between
     - orange: Qualisys and NN
     - green: Qualisys and Xsens
     Each pair refers to one DOF
     horizontal axis: absolute angle differences (rad)
    """)
    plot_angle_diffbox(rmse)
        
    # print("""
    # (mean-angledof-absdiff.pdf) Mean of absolute angle DOF differences over time between
    #  - orange: Qualisys and NN
    #  - green: Qualisys and Xsens
    #  x axis: time (s)
    #  y axis: absolute angle differences (rad)
    # """)
    # plot_dofmeans_angles()

    print("""
    (angle-errors.pdf) Per-segment orientation angle errors over time between
     - orange: Qualisys and NN
     - green: Qualisys and Xsens
     x axis: time (s)
     y axis: angle between segment orientations (rad)
    """)
    multiplot_angle_errors()

    print("""
    (angle-error-box.pdf) Box plots of per-segment orientation angle errors over time between
     - orange: Qualisys and NN
     - green: Qualisys and Xsens
     Each pair refers to one segment orientation
     horizontal axis: angle between segment orientations (rad)
    """)
    plot_angle_error_box()

    print("""
    (mean-angle-errors.pdf) Mean of per-segment orientation angle errors over time between
     - orange: Qualisys and NN
     - green: Qualisys and Xsens
     x axis: time (s)
     y axis: mean angle between segment orientations (rad)
    """)
    plot_mean_angle_errors()

    print("""
    (translationdof.pdf) Floating base translation DOF values over time.
     (Only Qualisys and Xsens, NN is not plotted because it can't predict fb translations)
     x axis: time (s)
     y axis: translation delta w.r.t. start point (m)
    """)
    multiplot_translation()

    print("""
    (translationdof-absdiff.pdf) Absolute difference of floating base translation
    DOF values (x, y, z) over time between Qualisys and Xsens
      x axis: time (s)
      y axis: absolute difference between translation deltas w.r.t. start point (m)
    """)
    multiplot_translation_metric(abs_difference)

    print("""
    (translationdof-absdiff-box.pdf) Box plot of absolute differences of floating base translation
    DOF values (x, y, z) over time between Qualisys and Xsens
     x axis: time (s)
     y axis: absolute difference between translation deltas w.r.t. start point (m)
    """)
    plot_translation_diffbox(abs_difference)

    print("""
    (trajectory.pdf) Floating base trajectories projected onto the xy plane, discarding z information
     (NN trajectory is not plotted because it can't predict fb translations)
     x axis: x coordinates (DOF 1)
     y axis: y coordinates (DOF 2)
    """)
    plot_translation_trajectory()

    print("""
    (scalar-metrics.txt) Dictionaries of different scalar values:
     - Per-DOF scalar median over time of absolute difference between translation DOFs between Qualisys and Xsens
     - Per-DOF scalar median over time of absolute difference between angle DOFs between
       - orange: Qualisys and NN
       - green: Qualisys and Xsens
     - Slope of linear regression fit to time series of mean absolute differences
       between DOFs (angle, translations) evaluated against Qualisys
    """)
    write_scalar_metrics(abs_difference)


    print("""
    (syncani-{qualisys,xsens,nn}.csv) Postprocessed and synchronized animations as Puppeteer-compatible csvs
    """)
    np.savetxt(f'{outpath}/syncani-qualisys.csv', recordings['Qualisys'], delimiter=', ', fmt='%1.4e')
    np.savetxt(f'{outpath}/syncani-xsens.csv', recordings['Xsens'], delimiter=', ', fmt='%1.4e')
    np.savetxt(f'{outpath}/syncani-nn.csv', recordings['NN'], delimiter=', ', fmt='%1.4e')


    # import IPython ; IPython.embed()
