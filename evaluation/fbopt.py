
#%%
import numpy as np
import matplotlib.pyplot as plt
import transforms3d as tf
from scipy.interpolate import interp1d

#Load data 
# file_QTM = './goofy_qualisys.csv'
# file_Xsens = './goofy_xsens.csv'

# file_QTM = '/home/m4/Dropbox/bsc/Martin_align_data/goofy_qualisys.csv'
# file_Xsens = '/home/m4/Dropbox/bsc/Martin_align_data/goofy_xsens.csv'
# file_QTM = '/home/m4/Dropbox/bsc/normal_walk/eval/qualisys/animation.csv'
# file_Xsens = '/home/m4/Dropbox/bsc/normal_walk/eval/xsens/animation.csv'
# file_QTM = '/home/m4/Dropbox/bsc/Squat/eval/qualisys/animation.csv'
# file_Xsens = '/home/m4/Dropbox/bsc/Squat/eval/xsens/animation.csv'
file_QTM = '/home/m4/Dropbox/bsc/Slackline_front/eval/qualisys/animation.csv'
file_Xsens = '/home/m4/Dropbox/bsc/Slackline_front/eval/xsens/animation.csv'


# data_q = np.genfromtxt(file_QTM, delimiter=',', dtype=float, unpack = True)
# data_x = np.genfromtxt(file_Xsens, delimiter=',', dtype=float, unpack = True)
data_q = np.loadtxt(file_QTM, delimiter=',').T  # (d x t)
data_x = np.loadtxt(file_Xsens, delimiter=',').T

#Interpolate and reduce data to floating base position
FB_q = data_q[1:4]

def recenter(x):
    """Shift floating base to the origin (0, 0, 0)"""
    y = np.empty_like(x)
    for i in range(x.shape[0]):
        y[i] = x[i] - x[i, 0]
    return y

time = data_q[0]
x_nd = interp1d(data_x[0], data_x[:], kind='cubic', fill_value="extrapolate")
FB_x = np.zeros_like(FB_q)
for i in range(data_q.shape[1]):
    t = time[i]
    FB_x[:,i] = x_nd(t)[1:4]

FB_q = recenter(FB_q)
FB_x = recenter(FB_x)


#%%
#find optimal angle to rotate around z axis
nsamples = 5000


def corrcoeff_xy_from_rotation(rec, target, alpha):
    """Determine x and y correlation coefficients between `rec` and
    `target` angle series after rotating `rec` around the z axis
    by angle alpha."""
    rotmat = tf.euler.euler2mat(0, 0, alpha, 'sxyz')
    rec_rot = rotmat @ rec  # Rotate rec by alpha around z axis

    Rx = np.corrcoef(target[0], rec_rot[0])[0, 1]
    Ry = np.corrcoef(target[1], rec_rot[1])[0, 1]
    R = (Rx + Ry) / 2
    return Rx, Ry


def find_best_alpha(rec, target, nsamples=1000):
    """Find the optimal angle to rotate `rec` by to achieve maximum
    correlation between `rec` and `target`.
    Works by brute-force trying `nsamples` possible uniformly spaced angles
    between 0 and 2 pi."""
    best_alpha_for_x = 0
    best_alpha_for_y = 0
    best_alpha = 0
    best_Rx = 0
    best_Ry = 0
    best_Rm = 0
    for alpha in np.arange(0, 2 * np.pi, 2 * np.pi / nsamples):
        Rx, Ry = corrcoeff_xy_from_rotation(FB_x, FB_q, alpha)

        if (Rx > best_Rx):
            best_Rx = Rx
            best_alpha_for_x = alpha
        
        if (Ry > best_Ry):
            best_Ry = Ry
            best_alpha_for_y = alpha

        Rm = (Rx + Ry) / 2
        if (Rm > best_Rm):
            best_Rm = Rm
            best_alpha = alpha

    print(f'Best alpha for X: {best_alpha_for_x:.4f} with R = {best_Rx:.4f}')
    print(f'Best alpha for Y: {best_alpha_for_y:.4f} with R = {best_Ry:.4f}')
    print(f'Best alpha for X and Y: {best_alpha:.4f} with mean R = {best_Rm:.4f}')

    return best_alpha

alpha = find_best_alpha(FB_x, FB_q)


#%%
#find optimal angle to rotate around z axis
nsamples = 1000

def corrcoeff_xy_from_rotation(rec, target, alpha):
    """Determine x and y correlation coefficients between `rec` and
    `target` angle series after rotating `rec` around the z axis
    by angle alpha."""
    rotmat = tf.euler.euler2mat(0, 0, alpha, 'sxyz')
    rec_rot = rotmat @ rec  # Rotate rec by alpha around z axis

    Rx = np.corrcoef(target[0], rec_rot[0])[0, 1]
    Ry = np.corrcoef(target[1], rec_rot[1])[0, 1]
    R = (Rx + Ry) / 2
    return Rx, Ry


def find_best_alpha(rec, target, nsamples=1000):
    """Find the optimal angle to rotate `rec` by to achieve maximum
    correlation between `rec` and `target`.
    Works by brute-force trying `nsamples` possible uniformly spaced angles
    between 0 and 2 pi."""
    best_alpha_for_x = 0
    best_alpha_for_y = 0
    best_alpha = 0
    best_Rx = 0
    best_Ry = 0
    best_Rm = 0
    best_err = np.inf
    for alpha in np.arange(0, 2 * np.pi, 2 * np.pi / nsamples):
        Rx, Ry = corrcoeff_xy_from_rotation(FB_x, FB_q, alpha)

        if (Rx > best_Rx):
            best_Rx = Rx
            best_alpha_for_x = alpha
        
        if (Ry > best_Ry):
            best_Ry = Ry
            best_alpha_for_y = alpha

        Rm = (Rx + Ry) / 2
        if (Rm > best_Rm):
            best_Rm = Rm
            best_alpha = alpha


    print(f'Best alpha for X: {best_alpha_for_x:.4f} with R = {best_Rx:.4f}')
    print(f'Best alpha for Y: {best_alpha_for_y:.4f} with R = {best_Ry:.4f}')
    print(f'Best alpha for X and Y: {best_alpha:.4f} with mean R = {best_Rm:.4f}')

    return best_alpha

alpha = find_best_alpha(FB_x, FB_q)


#%%

nsamples = 50
#function to compute axis angle from a matrix 
def errorFromMatrix(R):
    l = np.array([R[2,1]- R[1,2], R[0,2]- R[2,0] , R[1,0]- R[0,1] ])
    ang = np.arctan2(np.linalg.norm(l),( R[0,0] + R[1,1] + R[2,2] -1.0))  
    return ang

def rotmat_angle_error(R1, R2):
    R = R1.T @ R2
    l = np.array([R[2,1]- R[1,2], R[0,2]- R[2,0] , R[1,0]- R[0,1] ])
    ang = np.arctan2(np.linalg.norm(l),( R[0,0] + R[1,1] + R[2,2] -1.0))  
    return ang

FB_q_rot = data_q[4:7]
FB_x_rot = np.zeros_like(FB_q_rot)
for i in range(np.shape(data_q)[1]):
    t = time[i]
    FB_x_rot[:,i] = x_nd(t)[4:7]

best_err = np.inf
for alpha in np.arange(0, 2 * np.pi, 2 * np.pi / nsamples):
    
    for t in range(FB_q_rot.shape[1]):
        qmat = tf.euler.euler2mat(*FB_q_rot[:, t], 'syxz')
        xmat = tf.euler.euler2mat(*FB_x_rot[:, t], 'syxz')
        err = rotmat_angle_error(xmat, qmat)
        if err < best_err:
            best_err = err
            best_alpha = alpha

print(best_alpha, best_err)

#%%
    
#align angles for given alpha, create rotation matrix
alpha = 1.64
rot = tf.euler.euler2mat(0,0, alpha, 'sxyz')    

#save error per frame
error_values = np.zeros(np.shape(FB_q_rot)[1])

for i in range(FB_q_rot.shape[1]):
    # #Qualisys Data, Transform back and forth, not really necessary 
    R = tf.euler.euler2mat(FB_q_rot[2,i],FB_q_rot[1,i],FB_q_rot[0,i],'szxy')
    # ang = tf.euler.mat2euler(R,'szxy')
    # #save in correct order
    # assert np.all(np.isclose(FB_q_rot[:, i], ang[::-1]))
    # FB_q_rot[0,i] = ang[2]
    # FB_q_rot[1,i] = ang[1]
    # FB_q_rot[2,i] = ang[0]
    
    #Xsens Data, create Rotation Matrix, Angles are in yxz format -> extrinsic / intrinsic --> reverse order
    R_x = tf.euler.euler2mat(FB_x_rot[2,i],FB_x_rot[1,i],FB_x_rot[0,i],'szxy')
    #rotate by angle alpha
    R2 = np.dot(rot,R_x)
    # save
    ang = tf.euler.mat2euler(R2,'szxy')
    FB_x_rot[0,i] = ang[2]
    FB_x_rot[1,i] = ang[1]
    FB_x_rot[2,i] = ang[0]
    
    error = errorFromMatrix(np.dot(R.T, R2))
    error_values[i] = error
    
# plot stuff
figure = plt.figure(figsize=(10,10))
figure.set_rasterized(True)
ax1 = plt.subplot2grid((4, 2), (0, 0))
ax2 = plt.subplot2grid((4, 2), (0, 1))
ax3 = plt.subplot2grid((4, 2), (1, 0))
ax4 = plt.subplot2grid((4, 2), (1, 1))
ax5 = plt.subplot2grid((4, 2), (2, 0))
ax6 = plt.subplot2grid((4, 2), (2, 1))
ax7 = plt.subplot2grid((4, 2), (3, 0), colspan =2)

ax1.plot(time,FB_q_rot[0], color = 'red', label = 'qtm')
ax1.plot(time,FB_x_rot[0], color = 'blue', label = 'xsens')
ax1.legend()


corr = np.corrcoef(FB_q_rot[0],FB_x_rot[0] )
ax2.scatter(FB_q_rot[0], FB_x_rot[0], label = "R = {:.2f}".format(corr[0,1]))
ax2.legend()

ax3.plot(time,FB_q_rot[1], color = 'red')
ax3.plot(time,FB_x_rot[1], color = 'blue')


corr = np.corrcoef(FB_q_rot[1],FB_x_rot[1] )
ax4.scatter(FB_q_rot[1], FB_x_rot[1], label = "R = {:.2f}".format(corr[0,1]))
ax4.legend()

ax5.plot(time,FB_q_rot[2], color = 'red')
ax5.plot(time,FB_x_rot[2], color = 'blue')

#correlation looks funny because of +- 2Pi symmetry
corr = np.corrcoef(FB_q_rot[2],FB_x_rot[2] )
ax6.scatter(FB_q_rot[2], FB_x_rot[2], label = "R = {:.2f}".format(corr[0,1]))
ax6.legend()

#errors per frame from axis angle function is better than mean square because of +-2 Pi
ax7.plot(time,error_values * 180 / np.pi, label = 'Error per Frame in Degrees')
ax7.legend()


#%%



