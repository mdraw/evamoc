"""Prints all point labels contained in a c3d file."""

import c3d
import numpy as np
from numpy.linalg import norm

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('c3dfile')
args = parser.parse_args()

fname = args.filename
reader = c3d.Reader(open(fname, 'rb'))

labels = [s.strip() for s in reader.point_labels]

print('\n'.join(labels))
