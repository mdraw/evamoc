#!/usr/bin/env python3

"""
Extracts virtual markers and their relative positions to body segments
from an mvnx file (exported from Xsens MVN), writes them into a string
representation that defines a valid Lua table and prints it to stdout.
This lua table can then be used to export markers to a HeiMan model
definition.
"""

# TODO: (Low-prio: We could also calculate segment lengths from mvnx info for HeiMan parametrization)
# TODO: Consistent naming (decide: either mnvx or xsens, make distinction to HeiMan stuff more clear)
# TODO: Check if the Shoulder-Neck-offset (CV7 - TV2) has been considered correctly in all positions

import argparse
import pprint
import xmltodict  # Requires pip install xmltodict
import numpy as np


parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('mvnx_filename')
args = parser.parse_args()

# fname = 'kevin_for_martin_after_crash_2_-005.mvnx'
fname = args.mvnx_filename

xsens_to_heiman_segdict = {
    'Head': 'Head',
    'L3': 'MiddleTrunk',
    'L5': 'MiddleTrunk',  # Extra handling
    'LeftFoot': 'FootLeft',
    'LeftForeArm': 'LowerArmLeft',
    'LeftHand': 'HandLeft',
    'LeftLowerLeg': 'ShankLeft',
    'LeftShoulder': 'ClaviculaLeft',
    'LeftToe': 'FootLeft',  # Extra handling
    'LeftUpperArm': 'UpperArmLeft',
    'LeftUpperLeg': 'ThighLeft',
    'Neck': 'Neck',
    'Pelvis': 'Pelvis',
    'RightFoot': 'FootRight',
    'RightForeArm': 'LowerArmRight',
    'RightHand': 'HandRight',
    'RightLowerLeg': 'ShankRight',
    'RightShoulder': 'ClaviculaRight',
    'RightToe': 'FootRight',  # Extra handling
    'RightUpperArm': 'UpperArmRight',
    'RightUpperLeg': 'ThighRight',
    'T12': '_TODO',  # Extra handling
    'T8': '_TODO',  # Extra handling
}
# Inverted mapping (note that non-unique values result in multiple
#  reassignements and therefore in missing keys)
heiman_to_xsens_segdict = {h: x for x, h in xsens_to_heiman_segdict.items()}


# Unavailable marker names (in addition to joint markers, which start with j)
blacklist = {
    'pCentralButtock', 'pLeftSIPS', 'pRightSIPS',
    'pLeftTopOfFoot', 'pRightTopOfFoot', 'pLeftHandPalm', 'pRightHandPalm',
}


def is_arm_segment(segname: str) -> bool:
    if 'Arm' in segname or 'Hand' in segname:
        return True
    return False


def is_toe_segment(segname: str) -> bool:
    return 'Toe' in segname


def is_spinal_segment(segname: str) -> bool:
    return segname in {'L3', 'L5', 'T12', 'T8'} or 'Trunk' in segname


def find_segment(segments: dict, segname: str) -> dict:
    for segment in segments:
        if segment['@label'] == segname:
            return segment
    return KeyError('Not found.')


with open(fname) as f:
    doc = xmltodict.parse(f.read())

# We only care about this part of this subtree of the xml
segments = doc['mvnx']['subject']['segments']['segment']

# TODO: This search can be optimized by replacing it by a static lookup table
def find_marker_loc(mvnx_segname: str, marker_name: str, segments=segments) -> np.ndarray:
    for segment in segments:
        if segment['@label'] == mvnx_segname:
            markers = segment['points']['point']
            for point in markers:
                # Each point is one exported relative marker location 
                label = point['@label']
                if label == marker_name:
                    marker_location_str = point['pos_b']
                    x, y, z = tuple(float(coord) for coord in marker_location_str.split())
                    return np.array([x, y, z])
            raise KeyError(f'Marker {marker_name} not found.')
    raise KeyError(f'Segment {mvnx_segname} not found.')

# Dict of {segment_name: {marker_name: marker_location}
seg_dict = {h: {} for h in heiman_to_xsens_segdict.keys()}
# seg_dict = {
    # 'Pelvis': {},
    # 'Thigh_R': {},
    # 'Thigh_L': {},
    # 'Shank_R': {},
    # 'Shank_L': {},
    # 'Foot_R': {},
    # 'Foot_L': {},
    # 'MiddleTrunk': {},
    # 'UpperTrunk': {},
    # 'UpperArm_R': {},
    # 'UpperArm_L': {},
    # 'LowerArm_L': {},
    # 'LowerArm_R': {},
    # 'Hand_R': {},
    # 'Hand_L': {},
    # 'Head': {},
# }

# TODO: Construct spine

# Fill seg_dict with structured information about the marker locations
for segment in segments: 
    mvnx_segname = segment['@label']
    # Adjust segment names
    heiman_segname = xsens_to_heiman_segdict[mvnx_segname]
    if heiman_segname == '_TODO':
        continue
    markers = segment['points']['point']
    # if not (is_toe_segment(heiman_segname) or is_spinal_segment(heiman_segname)):
        # Toes are merged with feet, so we don't want a dict entry for them
        # Spinal segments are translated to HeiMan's upper and middle trunk
        # All other segments get their own dicts here
        # seg_dict[heiman_segname] = {}
    for point in markers:
        # Each point is one exported relative marker location 
        marker_name = point['@label'] 
        marker_location_str = point['pos_b']
        x, y, z = tuple(float(coord) for coord in marker_location_str.split())
        xyz = np.array([x, y, z])
        if not (marker_name in blacklist or marker_name.startswith('j')):
            # Only export markers that are available in c3d exports
            if is_arm_segment(heiman_segname):
                # The local coordinate systems of the arm
                #  segments are different from the HeiMan system. Using x, y, z
                #  as usual would lead to wrong marker locations.
                #  Explanation: HeiMan uses N-Pose and Xsens uses T-Pose reference
                if 'Right' in heiman_segname:
                    seg_dict[heiman_segname][marker_name] = x, z, y
                elif 'Left' in heiman_segname:
                    seg_dict[heiman_segname][marker_name] = x, z, -y
            elif 'Toe' in mvnx_segname:
                # Handle toes, which don't exist in HeiMan, by merging them into feet
                foot_name = heiman_segname.replace('Toe', 'Foot')
                if marker_name in seg_dict[foot_name]:
                    # No need to reassign foot markers that already exist
                    continue
                side = mvnx_segname[:-3]
                assert side in {'Left', 'Right'}
                # Assuming that seg_dict[foot_name] is already filled due to 
                #  iteration in alphabetic order
                m1 = find_marker_loc(mvnx_segname, f'p{side}FirstMetatarsal')
                m5 = find_marker_loc(mvnx_segname, f'p{side}FifthMetatarsal')
                # Point between metatarsals (origin of toe segment according
                #  to MVN user manual, section 22.6.11)
                toe_offset = (m1 + m5) / 2
                # Coordinates in the foot segment's reference frame (shifting
                #  the origin from between the metarsals to the ankle joint)
                fx, fy, fz = xyz + toe_offset
                seg_dict[foot_name][marker_name] = fx, fy, fz
            elif is_spinal_segment(heiman_segname):
                if mvnx_segname == 'L5':
                    newname = 'MiddleTrunk'
                    # if marker_name in seg_dict[newname]:
                        # continue
                    # Xsens L5 and HeiMan MiddleTrunk share the same origin
                    #  (the L5 joint), so we don't have to shift the coords
                    seg_dict[newname][marker_name] = x, y, z
                elif mvnx_segname == 'L3':
                    # L5 is relative to the Pelvis segment in HeiMan
                    newname = 'MiddleTrunk'
                    # if marker_name in seg_dict[newname]:
                        # continue
                    # Get (L5 -> L3) translation vector
                    offset = find_marker_loc('L5', 'jL4L3')
                    fx, fy, fz = xyz + offset
                    # import IPython ; IPython.embed(); raise SystemExit
                    # print(seg_dict[newname], flush=True)
                    seg_dict[newname][marker_name] = fx, fy, fz
            else:
                seg_dict[heiman_segname][marker_name] = x, y, z

# Python dictionary representation
pretty_pydict = pprint.pformat(seg_dict, indent=4, width=100, compact=False)
# Lua table representation of the same data
pretty_luatable = pretty_pydict\
    .replace('(', '{').replace(')', '}')\
    .replace('\'', '').replace(':', ' =')
# Formulate as a valid lua expression assignment for easy copy-pasting/Lua dofile() calls
pretty_luatable = 'markers_xsens = {\n ' + pretty_luatable[1:-1] + '\n}'
pretty_luatable = pretty_luatable.replace(' {   ', ' {    ')
info_header = f'-- Auto-extracted marker information from {fname}\n-- Generated by {__file__}\n'
pretty_luatable = info_header + pretty_luatable

print(pretty_luatable)
