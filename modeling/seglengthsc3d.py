# Original code by Kevin Stein, minor modifications by Martin Drawitsch
# Based on Kevin's c3d/Untitled.ipynb

import c3d
import numpy as np
from numpy.linalg import norm

# reader = c3d.Reader(open('Static_FB_1.c3d', 'rb'))
fname = '/home/m4/Dropbox/bsc/measurement_kevin_01-19/kevin_for_martin_after_crash_2_-005.c3d'
# fname = '/home/m4/Dropbox/bsc/measurement_kevin_01-19/kevin_goofy-walk_0002.c3d'
# fname = '/home/m4/Dropbox/bsc/measurement_kevin_01-19/kevin_static_0001.c3d'
reader = c3d.Reader(open(fname, 'rb'))

labels = [s.strip() for s in reader.point_labels]

Age = 24
Height =  1.835
Weight = 73
Gender = 'Male'
ShoulderWidth = 0
ShoulderNeckOffset = 0
HipCenterWidth = 0
FootWidth_R = 0
FootWidth_L = 0
HeelXOffset_R = 0
HeelXOffset_L = 0
HeelZOffset_R = 0
HeelZOffset_L = 0
Thigh_R = 0
Thigh_L = 0
Shank_L = 0
Shank_R = 0
MiddleTrunk = 0
UpperTrunk = 0
UpperArm_L = 0
UpperArm_R = 0
LowerArm_L = 0
LowerArm_R = 0
Hand_R = 0
Hand_L = 0
Head = 0
Pelvis = 0

if (Gender == 'Male'):
    Pelvis = Height * 0.0837
else:
    Pelvis = Height * 0.1046
    
Foot = 0.265


for i, p, analog in reader.read_frames():
    points = {s:p[labels.index(s)] for s in labels}
    S_offset = 2*norm((points['CV7'] - points['TV2'])[2])
    
    HipCenterWidth += norm((points['R_IAS'] - points['L_IAS'])[0:3])
    ShoulderWidth += norm((points['R_SAE'] - points['L_SAE'])[0:3])
    
    ShoulderNeckOffset += 2*norm((points['CV7'] - points['TV2'])[2])
    
    FootWidth_R += norm((points['R_FM5'] - points['R_FM1'])[0:3])
    FootWidth_L += norm((points['L_FM5'] - points['L_FM1'])[0:3])
    
    MiddleTrunk += norm((points['MAI']    - points['LV5'])[0:3])
    UpperTrunk += norm((points['MAI'] - points['CV7'])[0:3]) + S_offset/3.
    
    Thigh_R += norm((points['LV5'] - points['R_FLE'])[2]) - Pelvis*1000
    Thigh_L += norm((points['LV5'] - points['L_FLE'])[2]) - Pelvis*1000
    Shank_L += norm((points['R_FAL'] - points['R_FLE'])[0:3])
    Shank_R += norm((points['L_FAL'] - points['L_FLE'])[0:3])

    HeelXOffset_R += norm((points['R_FCC'] - points['R_FAL'])[0])
    HeelXOffset_L += norm((points['L_FCC'] - points['L_FAL'])[0])   
    HeelZOffset_R += norm((points['R_FCC'] - points['R_FAL'])[2])
    HeelZOffset_L += norm((points['L_FCC'] - points['L_FAL'])[2])
    
    UpperArm_L += norm((points['CV7'] - points['L_HLE'])[2])- 2*S_offset/3.
    UpperArm_R += norm((points['CV7'] - points['R_HLE'])[2])- 2*S_offset/3.
    
    LowerArm_L += norm((points['L_HLE'] - points['L_USP'])[0:3])
    LowerArm_R += norm((points['R_HLE'] - points['R_USP'])[0:3])
    Hand_R += 2*norm((points['R_HM2'] - points['R_RSP'])[0:3])
    Hand_L += 2*norm((points['L_HM2'] - points['L_RSP'])[0:3])
    Head += Height * 1000 - points['CV7'][2] - S_offset/3.
    
normalize = 1000 * i

Thigh = (Thigh_R + Thigh_L)/2.
Shank = (Shank_L + Shank_R)/2.
UpperArm = (UpperArm_L + UpperArm_R)/2.
LowerArm = (LowerArm_L + LowerArm_R)/2.
Hand = (Hand_R + Hand_L)/2.
HeelXOffset = (HeelXOffset_L + HeelXOffset_R)/2.
HeelZOffset = (HeelZOffset_L + HeelZOffset_R)/2.
FootWidth = (FootWidth_R + FootWidth_L)/2.

f = open("SegLen.txt", "w")
f.write('Pelvis, ' + str(Pelvis) + "\n")
f.write('Thigh_R, ' + str(Thigh / normalize) + "\n")
f.write('Thigh_L, ' + str(Thigh / normalize) + "\n")
f.write('Shank_R, ' + str(Shank / normalize) + "\n")
f.write('Shank_L, ' + str(Shank / normalize) + "\n")
f.write('Foot_R, ' + str(Foot) + "\n")
f.write('Foot_L, ' + str(Foot) + "\n")
f.write('MiddleTrunk, ' + str(MiddleTrunk / normalize) + "\n")
f.write('UpperTrunk, ' + str(UpperTrunk / normalize) + "\n")
f.write('UpperArm_R, ' + str(UpperArm / normalize) + "\n")
f.write('UpperArm_L, ' + str(UpperArm / normalize) + "\n")
f.write('LowerArm_L, ' + str(LowerArm / normalize) + "\n")
f.write('LowerArm_R, ' + str(LowerArm / normalize) + "\n")
f.write('Hand_R, ' + str(Hand / normalize) + "\n")
f.write('Hand_L, ' + str(Hand / normalize) + "\n")
f.write('Head, ' + str(Head / normalize) + "\n")
f.close()

g = open("Antro.txt", "w")
g.write('Age, ' + str(Age) + "\n")
g.write('Height, ' + str(Height) + "\n")
g.write('Gender, ' + str(Gender) + "\n")
g.write('Weight, ' + str(Weight) + "\n")
g.write('ShoulderWidth, ' + str(ShoulderWidth / normalize) + "\n")
g.write('ShoulderNeckOffset, ' + str(ShoulderNeckOffset / normalize) + "\n")
g.write('HipCenterWidth, ' + str(HipCenterWidth / normalize) + "\n")
g.write('FootWidth, ' + str(FootWidth / normalize) + "\n")
g.write('HeelXOffset, ' + str(HeelXOffset / normalize) + "\n")
g.write('HeelZOffset, ' + str(HeelZOffset / normalize) + "\n")
g.close()
